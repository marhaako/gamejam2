# Weekly Game Jam Week 119, "Tamagotchi"
Author: Marius Håkonsen (https://bitbucket.org/marhaako)

This is my contribution to the weekly game jam found here: https://itch.io/jam/weekly-game-jam-119

My goal for the Game Jam was to explore Unity and C#. This game is my first ever experience in both Unity and 
in C#. 

## Run
The .exe file to run the game is located in GameJam2/GameJam2/GameLauncher/GameJam2.exe

## Creation
The game itself is created using Unity. The logical scripts are written in C#.

Sounds effects are made in FLStudio.