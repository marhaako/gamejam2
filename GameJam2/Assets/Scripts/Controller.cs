﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;
using System.Threading.Tasks;
using System;
using System.Timers;

public class Controller : MonoBehaviour
{
    //CONSTANTS
    public const int START_HAPPY = 7;
    public const int START_FOOD = 1;
    public const int START_DRINK = 7;
    public const int START_SLEEP = 7;
    public const int START_HYGIENE = 5;
    public const float WAIT_TIME = 6f;
    public const float STAT_DECREASE_TIME = 60f;
    public const int REMOVE_SICKNESS_CHANCE = 20; //percent chance to remove flu
    public const int GET_SICKNESS_CHANCE = 5;
    public const int LIMIT_GET_ANGRY = 3; //If any level is below this, player gets angry with you


    //GAME OBJECTS
    List<GameObject> actions = new List<GameObject>();
    public GameObject food;
    public GameObject play;
    public GameObject sleep;
    public GameObject beer;
    public GameObject medicine;
    public GameObject music;
    public GameObject shower;
    public GameObject cashGame;
    public GameObject musicGame;
    public GameObject student;


    //AUDIO SOURCES
    public AudioSource unhappy_sound;
    public AudioSource sick_sound;
    public AudioSource notsick_sound;
    public AudioSource death_sound;
    public AudioSource reset_sound;
    public AudioSource drink_sound;
    public AudioSource sleep_sound;
    public AudioSource eat_sound;
    public AudioSource shower_sound;




    //HELP VARIABLES AND BOOLS
    int selected = 0;
    public bool animPlaying = false;
    bool isDead = false;
    bool isSick = false;


    //HEALTH STATS
    int happyLevel = START_HAPPY;
    int foodLevel = START_FOOD;
    int drinkLevel = START_DRINK;
    int sleepLevel = START_SLEEP;
    int hygieneLevel = START_HYGIENE;


    // Start is called before the first frame update
    void Start()
    {
        //Set up game
        ResetGameState();

        //Add menu items to actions list
        actions.Add(food);
        actions.Add(play);
        actions.Add(beer);
        actions.Add(sleep);
        actions.Add(medicine);
        actions.Add(music);
        actions.Add(shower);
        ShowSelected();
    }

    // Update is called once per frame
    void Update()
    {
        
        if(!animPlaying && !cashGame.active) { //Lock input if animation playing
            if (Input.GetKeyDown (KeyCode.Space)) {
                UpdateCharacter();
            } else if (Input.GetKeyDown (KeyCode.LeftArrow)) {
                SelectPrevious();
            } else if (Input.GetKeyDown (KeyCode.RightArrow)) {
                SelectNext();
            }        
        } else if(isDead) {
            if (Input.GetKeyDown (KeyCode.Space)) {
                ResetGameState();
            }  
        }
    }

    void ResetGameState() {
        isDead = false;
        happyLevel = START_HAPPY;
        foodLevel = START_FOOD;
        drinkLevel = START_DRINK;
        sleepLevel = START_SLEEP;
        hygieneLevel = START_HYGIENE;
        isSick = false;
        animPlaying = true;
        int selected = 0;

        student.SetActive(true);
        cashGame.SetActive(false);
        musicGame.SetActive(false);

        CancelInvoke();
        InvokeRepeating ("DecreaseStats", STAT_DECREASE_TIME, STAT_DECREASE_TIME);
        InvokeRepeating ("PlayIdleAnimation", WAIT_TIME, WAIT_TIME);

        reset_sound.Play();
        student.GetComponent<Animator>().Play("Cool", -1, 0f);
        ReturnToIdleAfterPlayingAnimation(2000);

    }
    void SelectNext() {
        if(selected + 1 > actions.Count - 1) {
            selected = 0;
        } else {
            selected += 1;
        }
        ShowSelected();
    }

    void SelectPrevious() {
        if(selected == 0) {
            selected = actions.Count - 1;
        } else {
            selected -= 1;
        }
        ShowSelected();
    }

    void ShowSelected() {
        foreach(var item in actions) {
            item.SetActive(false);
        }
        actions[selected].SetActive(true);
    }

    void UpdateCharacter() {
        if (actions[selected].name == "beer") {
            PlaydrinkAnimation();
        } else if (actions[selected].name == "sleep") {
            PlaySleepAnimation();
        } else if (actions[selected].name == "food") {
            PlayEatAnimation();
        } else if (actions[selected].name == "play") {
            PlayCashGame();
        } else if (actions[selected].name == "medicine") {
            PlayMedicineAnimation();
        } else if (actions[selected].name == "music") {
            PlayMusicGame();
        } else if (actions[selected].name == "shower") {
            PlayShowerAnimation();
        }
    }


    void PlaydrinkAnimation() {
        animPlaying = true;
        if(drinkLevel < 8) {
            drinkLevel += 3;
            foodLevel--; 
        }
        drink_sound.Play();
        student.GetComponent<Animator>().Play("Drink", -1, 0f);
        ReturnToIdleAfterPlayingAnimation(5000);
    }

    void PlayIdleAnimation() {
        if(!animPlaying) {
            student.GetComponent<Animator>().Play("Blink", -1, 0f);
        }
    }

    void PlaySleepAnimation() {
        if(sleepLevel < 9) {
            sleepLevel += 2;
        }
        animPlaying = true;
        sleep_sound.Play();
        student.GetComponent<Animator>().Play("Sleep", -1, 0f);
        ReturnToIdleAfterPlayingAnimation(5000);

    }

    void PlayCashGame() {
        bool playing = true;
        animPlaying = true;
        student.SetActive(false); //Remove student from screen
        cashGame.SetActive(true); //Show game on screen
        sleepLevel--;
    }

    void PlayEatAnimation() {
        animPlaying = true;
        if(foodLevel < 9) {
            foodLevel += 2;
        }
        eat_sound.Play();
        student.GetComponent<Animator>().Play("Eat", -1, 0f);
        ReturnToIdleAfterPlayingAnimation(5000);

    }

    void PlayMedicineAnimation() {
        animPlaying = true;
        if(isSick) { //If sick_sound, chance of removing sickness
            System.Random rnd = new System.Random();
            var num = rnd.Next(1, 100);
            if(num > 100-REMOVE_SICKNESS_CHANCE) {
                isSick = false;
                notsick_sound
        .Play();
                student.GetComponent<Animator>().Play("Medicine", -1, 0f);
                ReturnToIdleAfterPlayingAnimation(3000);
            } else {
                student.GetComponent<Animator>().Play("Medicine", -1, 0f);
                ReturnToIdleAfterPlayingAnimation(3000);
            }
        } else {
            student.GetComponent<Animator>().Play("Medicine", -1, 0f);
            ReturnToIdleAfterPlayingAnimation(5000);
        } 

        
    }

    void PlayShowerAnimation() {
        animPlaying = true;
        shower_sound.Play();
        student.GetComponent<Animator>().Play("Shower", -1, 0f);
        hygieneLevel = 10;
        ReturnToIdleAfterPlayingAnimation(5000);

    }

    void PlayMusicGame() {
        animPlaying = true;
        student.SetActive(false); //Remove student from screen
        musicGame.SetActive(true); //Show game on screen
        sleepLevel--;
    }

    async Task ReturnToIdleAfterPlayingAnimation(int millisec) {
        await Task.Delay(millisec);
        animPlaying = false;
        PlayIdleAnimation();
    }

    void DecreaseStats() {
        if(!animPlaying && !isDead) {
            happyLevel--;
            foodLevel--;
            drinkLevel--;
            hygieneLevel--;
            if(drinkLevel == 0 || foodLevel == 0) { //If food or drink level reaches 0, player dies
                animPlaying = true;
                student.GetComponent<Animator>().Play("Death", -1, 0f);
                death_sound.Play();
                isDead = true;
            }
            else if(isSick) {  //If player is sick_sound, show Sick animation + sound
                animPlaying = true;
                student.GetComponent<Animator>().Play("Sick", -1, 0f);
                ReturnToIdleAfterPlayingAnimation(3000);
            } 
            else if(drinkLevel < LIMIT_GET_ANGRY || foodLevel < LIMIT_GET_ANGRY || drinkLevel < LIMIT_GET_ANGRY || sleepLevel < LIMIT_GET_ANGRY) { //IF player is starting to get unhealthy
                animPlaying = true;                                        //play the Angry animation and make a sound
                student.GetComponent<Animator>().Play("Angry", -1, 0f);
                unhappy_sound.Play();
                ReturnToIdleAfterPlayingAnimation(3000);
            } 


            if(!isSick) { //Random chance of getting sick every tick, if not already sick
                System.Random rnd = new System.Random();
                var num = rnd.Next(1, 100);
                if(num < GET_SICKNESS_CHANCE) {
                    isSick = true;
                }
            } 
        }
    }


}
