﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;
using System.Threading.Tasks;
using System;

public class MusicGameController : MonoBehaviour
{
    //GAME OBJECTS
    public GameObject game;
    public GameObject tamagotchi;
    public GameObject student;

    //AUDIO SOURCES
    public AudioSource song1, song2, song3;
    public AudioSource drum1, drum2;
    List<AudioSource> songs = new List<AudioSource>();

    //HELP VARIABLES
    bool resetGame = false;
    int currentSong = 0;
    



    // Start is called before the first frame update
    void Start()
    {
        songs.Add(song1);
        songs.Add(song2);
        songs.Add(song3);

        songs[currentSong].Play();

        EndGameAfterSongFinishes((int)songs[currentSong].clip.length*1000 + 1000);

    }

    // Update is called once per frame
    void Update()
    {
        if(resetGame) {

            currentSong++;
            if(currentSong > songs.Count -1) {
                currentSong = 0;
            }

            songs[currentSong].Play();
            EndGameAfterSongFinishes((int)songs[currentSong].clip.length*1000 + 1000);
            resetGame = false;
        }
        if(game.active){
            if (Input.GetKeyDown (KeyCode.LeftArrow)) {
                LeftAction();
            } else if (Input.GetKeyDown (KeyCode.RightArrow)) {
                RightAction();
            } else if (Input.GetKeyDown (KeyCode.Space)) {
                EndGame();
            }
        }
        
    }

    void LeftAction() {
        drum1.Play();
    }

    void RightAction() {
        drum2.Play();
    }


    async Task EndGameAfterSongFinishes(int millisec) {
        await Task.Delay(millisec);
        EndGame();
    }

    void EndGame() {
        game.SetActive(false);
        student.SetActive(true);
        tamagotchi.GetComponent<Controller>().animPlaying = false;
        resetGame = true;
    }

}
