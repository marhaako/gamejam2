﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CashGameController : MonoBehaviour
{
    //CONSTANTS
    public const float INITIAL_GAME_SPEED = 1.2f;
    public const float GAME_SPEED_LIMIT = 0.2f;
    public const float MULTIPLY_BY_EACH_ROUND = 0.9f; //1 = same fall speed each round

    //GAME OBJECTS
    public GameObject cash;
    public GameObject player;
    public GameObject game;
    public GameObject student;
    public GameObject tamagotchi;

    //AUDIO SOURCES
    public AudioSource point_sound;

    //COLLIDERS
    BoxCollider2D playerCollider, cashCollider;

    //HELP VARIABLES
    float gameSpeed = INITIAL_GAME_SPEED;


    // Start is called before the first frame update
    void Start()
    {
        ResetGameState();
        playerCollider = player.GetComponent<BoxCollider2D>();
        cashCollider = cash.GetComponent<BoxCollider2D>();

    }

    // Update is called once per frame
    void Update()
    {
        if(game.active){
            if (Input.GetKeyDown (KeyCode.Space)) {
                player.GetComponent<Animator>().Play("Grab", -1, 0f);
                CheckIntercepts();
            } else if (Input.GetKeyDown (KeyCode.LeftArrow)) {
                MovePlayerLeft();
            } else if (Input.GetKeyDown (KeyCode.RightArrow)) {
                MovePlayerRight();
            } 
        }
    
    }

    void CheckIntercepts() {
        if(playerCollider.bounds.Intersects(cashCollider.bounds)) {
            point_sound.Play();
            cash.transform.localPosition = new Vector3(RandomXPosition(), 0.8f, 0);
            if(gameSpeed > GAME_SPEED_LIMIT) {
                gameSpeed *= MULTIPLY_BY_EACH_ROUND;
            }
            CancelInvoke();
            InvokeRepeating ("MoveObjectiveDown", gameSpeed, gameSpeed);

        }
    }

    void MovePlayerLeft() {
        if(player.transform.localPosition.x - 0.2f >= -1.1f) {
            Vector3 temp = new Vector3(-0.2f, 0, 0);
            player.transform.localPosition += temp;
        }
    }

    void MovePlayerRight() {
        if(player.transform.localPosition.x + 0.2f <= 1.1f) {
            Vector3 temp = new Vector3(0.2f, 0, 0);
            player.transform.localPosition += temp;
        }
    }

    void ResetGameState() {
        player.transform.localPosition = new Vector3(0.1f, -0.4f, 0);
        cash.transform.localPosition = new Vector3(RandomXPosition(), 0.8f, 0);
        gameSpeed = INITIAL_GAME_SPEED;
        CancelInvoke();
        InvokeRepeating ("MoveObjectiveDown", gameSpeed, gameSpeed);

    }

    void MoveObjectiveDown() {
        if(cash.transform.localPosition.y - 0.2f <= -0.5f) { //Player lost
            game.SetActive(false);
            student.SetActive(true);
            tamagotchi.GetComponent<Controller>().animPlaying = false;
            ResetGameState();
        } else {
            if(game.active) {
            Vector3 temp = new Vector3(0, -0.2f, 0);
            cash.transform.localPosition += temp;
            }
        }
    }

    float RandomXPosition() { //Between -1.2 and 1.0, 0.2 intervals
        System.Random rnd = new System.Random();
        float numb = rnd.Next(1, 11);
        float number = -1.1f;
        number += numb * 0.2f;
        return number;
    }
}
